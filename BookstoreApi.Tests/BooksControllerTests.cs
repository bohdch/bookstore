using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using BookstoreApi.Controllers;
using BookstoreApi.Models;
using BookstoreApi.Data;

public class BooksControllerTests
{
    private BookstoreDbContext GetDbContext()
    {
        var options = new DbContextOptionsBuilder<BookstoreDbContext>()
            .UseInMemoryDatabase($"Bookstore-{Guid.NewGuid()}")
            .Options;

        var context = new BookstoreDbContext(options);
        return context;
    }

    [Fact]
    public async Task WhenSearchingBooks_ReturnsFilteredBooks()
    {
        // Arrange
        string queryTitle = "Harry Potter";
        string queryAuthor = "Rowling";
        string queryGenre = "Fantasy";

        var context = GetDbContext();
        SeedBooks(context, 10);
        var booksController = new BooksController(context);
        var book = new Book
        {
            Id = 11,
            Title = $"{queryTitle}",
            AuthorId = 1,
            Author = new Author { Name = queryAuthor },
            GenreId = 1,
            Genre = new Genre { Name = queryGenre },
            Price = 16.99M,
            QuantityAvailable = 10
        };
        context.Books.Add(book);
        context.SaveChanges();

        // Act
        var result = await booksController.GetBooks(queryTitle, queryAuthor, queryGenre);

        // Assert
        Assert.IsType<OkObjectResult>(result);
        var books = ((OkObjectResult)result).Value as List<Book>;
        Assert.Single(books);
        Assert.Equal(book.Id, books.First().Id);
    }

    [Fact]
    public async Task WhenAddingBook_SavesNewBook()
    {
        // Arrange
        var context = GetDbContext();
        var booksController = new BooksController(context);

        var newBook = new Book
        {
            Title = "New Book",
            AuthorId = 1,
            GenreId = 1,
            Price = 14.99M,
            QuantityAvailable = 10
        };

        // Act
        var result = await booksController.PostBook(newBook);

        // Assert
        Assert.IsType<CreatedAtActionResult>(result);
    }

    [Fact]
    public async Task WhenUpdatingBook_UpdatesBook()
    {
        // Arrange
        var context = GetDbContext();
        SeedBooks(context, 1);
        var booksController = new BooksController(context);

        var updateBook = new Book
        {
            Id = 1,
            Title = "Updated Book",
            AuthorId = 1,
            GenreId = 1,
            Price = 12.99M,
            QuantityAvailable = 10
        };

        // Act
        var result = await booksController.PutBook(1, updateBook);

        // Assert
        Assert.IsType<NoContentResult>(result);
    }

    [Fact]
    public async Task WhenUpdatingBook_ReturnsNotFound_IfBookDoesNotExist()
    {
        // Arrange
        var context = GetDbContext();
        var booksController = new BooksController(context);
        var updateBook = new Book
        {
            Id = 1,
            Title = "Updated Book",
            AuthorId = 1,
            GenreId = 1,
            Price = 12.99M,
            QuantityAvailable = 10
        };

        // Act
        var result = await booksController.PutBook(1, updateBook);

        // Assert
        Assert.IsType<NotFoundObjectResult>(result);
    }

    [Fact]
    public async Task WhenDeletingBook_DeletesBook()
    {
        // Arrange
        var context = GetDbContext();
        SeedBooks(context, 1);
        var booksController = new BooksController(context);

        // Act
        var result = await booksController.DeleteBook(1);

        // Assert
        Assert.IsType<NoContentResult>(result);
    }

    [Fact]
    public async Task WhenGettingAllBooks_ReturnsEmptyList_IfNoBooksExist()
    {
        // Arrange
        var context = GetDbContext();
        var booksController = new BooksController(context);

        // Act
        var result = await booksController.GetBooks();

        // Assert
        Assert.IsType<OkObjectResult>(result);
        var books = ((OkObjectResult)result).Value as List<Book>;
        Assert.Empty(books);
    }

    [Fact]
    public async Task WhenGettingBookById_ReturnsNotFound_IfBookDoesNotExist()
    {
        // Arrange
        var context = GetDbContext();
        var booksController = new BooksController(context);

        // Act
        var result = await booksController.GetBook(1);

        // Assert
        Assert.IsType<NotFoundResult>(result);
    }

    [Fact]
    public async Task WhenSearchingBooks_ReturnsEmptyList_IfNoMatchingBooks()
    {
        // Arrange
        var context = GetDbContext();
        SeedBooks(context, 5);
        var booksController = new BooksController(context);

        // Act
        var result = await booksController.GetBooks("NonExistentTitle", "NonExistentAuthor", "NonExistentGenre");

        // Assert
        Assert.IsType<OkObjectResult>(result);
        var books = ((OkObjectResult)result).Value as List<Book>;
        Assert.Empty(books);
    }

    [Fact]
    public async Task WhenDeletingBook_ReturnsNotFound_IfBookDoesNotExist()
    {
        // Arrange
        var context = GetDbContext();
        var booksController = new BooksController(context);

        // Act
        var result = await booksController.DeleteBook(1);

        // Assert
        Assert.IsType<NotFoundResult>(result);
    }

    private void SeedBooks(BookstoreDbContext context, int numberOfBooks)
    {
        for (int i = 0; i < numberOfBooks; i++)
        {
            context.Books.Add(new Book
            {
                Id = i + 1,
                Title = $"Book {i + 1}",
                AuthorId = 1,
                GenreId = 1,
                Price = 15.99M,
                QuantityAvailable = 10
            });
        }

        context.SaveChanges();
    }
}