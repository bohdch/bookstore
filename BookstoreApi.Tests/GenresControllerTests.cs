using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using BookstoreApi.Controllers;
using BookstoreApi.Models;
using BookstoreApi.Data;

public class GenresControllerTests
{
    private BookstoreDbContext GetDbContext()
    {
        var options = new DbContextOptionsBuilder<BookstoreDbContext>()
            .UseInMemoryDatabase($"Bookstore-{Guid.NewGuid()}")
            .Options;

        var context = new BookstoreDbContext(options);
        return context;
    }

    [Fact]
    public async Task WhenGettingAllGenres_ReturnsAllGenres()
    {
        // Arrange
        var context = GetDbContext();
        SeedGenres(context, 3);
        var genresController = new GenresController(context);

        // Act
        var result = await genresController.GetGenres();

        // Assert
        Assert.IsType<OkObjectResult>(result);
        var genres = ((OkObjectResult)result).Value as List<Genre>;
        Assert.Equal(3, genres.Count);
    }

    [Fact]
    public async Task WhenGettingGenreById_ReturnsCorrectGenre()
    {
        // Arrange
        var context = GetDbContext();
        SeedGenres(context, 1);
        var genresController = new GenresController(context);

        // Act
        var result = await genresController.GetGenre(1);

        // Assert
        Assert.IsType<OkObjectResult>(result);
        var genre = ((OkObjectResult)result).Value as Genre;
        Assert.Equal(1, genre.Id);
    }

    [Fact]
    public async Task WhenAddingGenre_SavesNewGenre()
    {
        // Arrange
        var context = GetDbContext();
        var genresController = new GenresController(context);

        var newGenre = new Genre
        {
            Name = "New Genre",
        };

        // Act
        var result = await genresController.PostGenre(newGenre);

        // Assert
        Assert.IsType<CreatedAtActionResult>(result);
    }

    [Fact]
    public async Task WhenUpdatingGenre_UpdatesGenre()
    {
        // Arrange
        var context = GetDbContext();
        SeedGenres(context, 1);
        var genresController = new GenresController(context);

        var updateGenre = new Genre
        {
            Id = 1,
            Name = "Updated Genre",
        };

        // Act
        var result = await genresController.PutGenre(1, updateGenre);

        // Assert
        Assert.IsType<NoContentResult>(result);
    }

    [Fact]
    public async Task WhenDeletingGenre_DeletesGenre()
    {
        // Arrange
        var context = GetDbContext();
        SeedGenres(context, 1);
        var genresController = new GenresController(context);

        // Act
        var result = await genresController.DeleteGenre(1);

        // Assert
        Assert.IsType<NoContentResult>(result);
    }

    private void SeedGenres(BookstoreDbContext context, int numberOfGenres)
    {
        for (int i = 0; i < numberOfGenres; i++)
        {
            context.Genres.Add(new Genre
            {
                Id = i + 1,
                Name = $"Genre {i + 1}",
            });
        }

        context.SaveChanges();
    }

    // Add to BooksControllerTests
    [Fact]
    public async Task WhenGettingInvalidBookById_ReturnsNotFound()
    {
        // Arrange
        var context = GetDbContext();
        var booksController = new BooksController(context);

        // Act
        var result = await booksController.GetBook(999);

        // Assert
        Assert.IsType<NotFoundResult>(result);
    }

    // Add to AuthorsControllerTests
    [Fact]
    public async Task WhenGettingInvalidAuthorById_ReturnsNotFound()
    {
        // Arrange
        var context = GetDbContext();
        var authorsController = new AuthorsController(context);

        // Act
        var result = await authorsController.GetAuthor(999);

        // Assert
        Assert.IsType<NotFoundResult>(result);
    }

    [Fact]
    public async Task WhenGettingInvalidGenreById_ReturnsNotFound()
    {
        // Arrange
        var context = GetDbContext();
        var genresController = new GenresController(context);

        // Act
        var result = await genresController.GetGenre(999);

        // Assert
        Assert.IsType<NotFoundResult>(result);
    }
}