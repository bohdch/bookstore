using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using BookstoreApi.Controllers;
using BookstoreApi.Models;
using BookstoreApi.Data;

public class AuthorsControllerTests
{
	private BookstoreDbContext GetDbContext()
	{
		var options = new DbContextOptionsBuilder<BookstoreDbContext>()
			.UseInMemoryDatabase($"Bookstore-{Guid.NewGuid()}")
			.Options;

		var context = new BookstoreDbContext(options);
		return context;
	}

	[Fact]
	public async Task WhenGettingAllAuthors_ReturnsAllAuthors()
	{
		// Arrange
		var context = GetDbContext();
		SeedAuthors(context, 4);
		var authorsController = new AuthorsController(context);

		// Act
		var result = await authorsController.GetAuthors();

		// Assert
		Assert.IsType<OkObjectResult>(result);
		var authors = ((OkObjectResult)result).Value as List<Author>;
		Assert.Equal(4, authors.Count);
	}

	[Fact]
	public async Task WhenGettingAuthorById_ReturnsCorrectAuthor()
	{
		// Arrange
		var context = GetDbContext();
		SeedAuthors(context, 1);
		var authorsController = new AuthorsController(context);

		// Act
		var result = await authorsController.GetAuthor(1);

		// Assert
		Assert.IsType<OkObjectResult>(result);
		var author = ((OkObjectResult)result).Value as Author;
		Assert.Equal(1, author.Id);
	}

	[Fact]
	public async Task WhenAddingAuthor_SavesNewAuthor()
	{
		// Arrange
		var context = GetDbContext();
		var authorsController = new AuthorsController(context);

		var newAuthor = new Author
		{
			Name = "New Author",
		};

		// Act
		var result = await authorsController.PostAuthor(newAuthor);

		// Assert
		Assert.IsType<CreatedAtActionResult>(result);
	}

	[Fact]
	public async Task WhenUpdatingAuthor_UpdatesAuthor()
	{
		// Arrange
		var context = GetDbContext();
		SeedAuthors(context, 1);
		var authorsController = new AuthorsController(context);

		var updateAuthor = new Author
		{
			Id = 1,
			Name = "Updated Author",
		};

		// Act
		var result = await authorsController.PutAuthor(1, updateAuthor);

		// Assert
		Assert.IsType<NoContentResult>(result);
	}

	[Fact]
	public async Task WhenDeletingAuthor_DeletesAuthor()
	{
		// Arrange
		var context = GetDbContext();
		SeedAuthors(context, 1);
		var authorsController = new AuthorsController(context);

		// Act
		var result = await authorsController.DeleteAuthor(1);

		// Assert
		Assert.IsType<NoContentResult>(result);
	}

	private void SeedAuthors(BookstoreDbContext context, int numberOfAuthors)
	{
		for (int i = 0; i < numberOfAuthors; i++)
		{
			context.Authors.Add(new Author
			{
				Id = i + 1,
				Name = $"Author {i + 1}",
			});
		}

		context.SaveChanges();
	}
}