# Bookstore API

Bookstore API is a RESTful API built with ASP.NET Core. It allows users to manage books, authors, and genres through simple CRUD (Create, Read, Update, and Delete) operations.

![Bookstore API](https://via.placeholder.com/800x300.png?text=Bookstore+API)

## Features

- Manage Books
- Manage Authors
- Manage Genres
- Search Books by Title, Author, and Genre
- Uses Entity Framework Core and In-Memory Database

## Getting Started

### Prerequisites

- [.NET SDK 6.0](https://dotnet.microsoft.com/download/dotnet/6.0)
- Visual Studio, Visual Studio Code, or other IDE with .NET Support

### Running the API locally

1. Clone the repo:

```bash
git clone https://github.com/yourusername/yourreponame.git
```

2. Change to the API directory:

```bash
cd yourreponame/BookstoreApi
```

3. Restore the necessary packages and start the API:

```bash
dotnet restore
dotnet run
```

The Bookstore API should now be accessible at `https://localhost:5001`.

### Running Tests

1. Change to the test directory:

```bash
cd yourreponame/BookstoreApiTests
```

2. Run the test suite:

```bash
dotnet restore
dotnet test
```

## Contributing

If you'd like to contribute to the project, please submit a pull request on GitHub. All contributions are welcome!

1. Fork the repo on GitHub
2. Clone your forked repo locally
3. Create a new branch for your feature/fix
4. Make your changes, and make sure the tests pass
5. Commit your changes and push to your fork
6. On GitHub, create a pull request to merge your changes into the upstream repository

